/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2011, Willow Garage, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "device.hpp"

namespace pcl
{
  namespace device_64
  {
    const float sigma_color = 30;          //in mm
    const float sigma_space_640 = 4.5;     // in pixels at VGA resolution

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    __global__ void
    bilateralKernel (const PtrStepSz<ushort> src, 
                     PtrStep<ushort> dst, 
                     float sigma_space2_inv_half, float sigma_color2_inv_half)
    {
      int x = threadIdx.x + blockIdx.x * blockDim.x;
      int y = threadIdx.y + blockIdx.y * blockDim.y;

      if (x >= src.cols || y >= src.rows)
        return;

      const int R = 6;       //static_cast<int>(sigma_space * 1.5);
      const int D = R * 2 + 1;

      int value = src.ptr (y)[x];

      int tx = min (x - D / 2 + D, src.cols - 1);
      int ty = min (y - D / 2 + D, src.rows - 1);

      float sum1 = 0;
      float sum2 = 0;

      for (int cy = max (y - D / 2, 0); cy < ty; ++cy)
      {
        for (int cx = max (x - D / 2, 0); cx < tx; ++cx)
        {
          int tmp = src.ptr (cy)[cx];

          float space2 = (x - cx) * (x - cx) + (y - cy) * (y - cy);
          float color2 = (value - tmp) * (value - tmp);

          float weight = __expf (-(space2 * sigma_space2_inv_half + color2 * sigma_color2_inv_half));

          sum1 += tmp * weight;
          sum2 += weight;
        }
      }

      int res = __float2int_rn (sum1 / sum2);
      dst.ptr (y)[x] = max (0, min (res, numeric_limits<short>::max ()));
    }

#define MEDIAN_BLOCK_W 8
#define MEDIAN_BLOCK_H 8

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    __global__ void
    medianKernel3x3 (const PtrStepSz<ushort> src,
                     PtrStep<ushort> dst)
    {
        // FIXME: far from being optimal.

        // Shared min/max
        __shared__ ushort window [MEDIAN_BLOCK_W*MEDIAN_BLOCK_H][9];

        unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
        unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

        unsigned int thread_id = threadIdx.y*blockDim.y + threadIdx.x;

        if (x >= (src.cols-1) || (y >= src.rows-1) || (x < 1) || (y < 1))
            return;

        window[thread_id][0] = src.ptr(y-1)[x - 1];
        window[thread_id][1] = src.ptr(y-1)[x];
        window[thread_id][2] = src.ptr(y-1)[x+1];
        window[thread_id][3] = src.ptr(y)[x-1];
        window[thread_id][4] = src.ptr(y)[x];
        window[thread_id][5] = src.ptr(y)[x+1];
        window[thread_id][6] = src.ptr(y+1)[x-1];
        window[thread_id][7] = src.ptr(y+1)[x];
        window[thread_id][8] = src.ptr(y+1)[x+1];
        syncthreads();

        // Order half of the elements.
        for (int j = 0; j < 5; ++j)
        {
            // Find new minimum.
            int min_index = j;
            for (unsigned int l = j+1; l < 9; ++l)
                if (window[thread_id][l] < window[thread_id][min_index])
                    min_index = l;

            // Put found minimum element in its place
            const ushort temp = window[thread_id][j];
            window[thread_id][j] = window[thread_id][min_index];
            window[thread_id][min_index] = temp;

            syncthreads();
        }

        dst.ptr (y)[x] = window[thread_id][4];
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    __global__ void
    pyrDownGaussKernel (const PtrStepSz<ushort> src, PtrStepSz<ushort> dst, float sigma_color)
    {
      int x = blockIdx.x * blockDim.x + threadIdx.x;
      int y = blockIdx.y * blockDim.y + threadIdx.y;

      if (x >= dst.cols || y >= dst.rows)
        return;

      const int D = 5;

      int center = src.ptr (2 * y)[2 * x];

      int x_mi = max(0, 2*x - D/2) - 2*x;
      int y_mi = max(0, 2*y - D/2) - 2*y;

      int x_ma = min(src.cols, 2*x -D/2+D) - 2*x;
      int y_ma = min(src.rows, 2*y -D/2+D) - 2*y;
            
      float sum = 0;
      float wall = 0;
      
      float weights[] = {0.375f, 0.25f, 0.0625f} ;

      for(int yi = y_mi; yi < y_ma; ++yi)
          for(int xi = x_mi; xi < x_ma; ++xi)
          {
              int val = src.ptr (2*y + yi)[2*x + xi];

              if (abs (val - center) < 3 * sigma_color)
              {                                 
                sum += val * weights[abs(xi)] * weights[abs(yi)];
                wall += weights[abs(xi)] * weights[abs(yi)];
              }
          }


      dst.ptr (y)[x] = static_cast<int>(sum /wall);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    __global__ void
    pyrDownKernel (const PtrStepSz<ushort> src, PtrStepSz<ushort> dst, float sigma_color)
    {
      int x = blockIdx.x * blockDim.x + threadIdx.x;
      int y = blockIdx.y * blockDim.y + threadIdx.y;

      if (x >= dst.cols || y >= dst.rows)
        return;

      const int D = 5;

      int center = src.ptr (2 * y)[2 * x];

      int tx = min (2 * x - D / 2 + D, src.cols - 1);
      int ty = min (2 * y - D / 2 + D, src.rows - 1);
      int cy = max (0, 2 * y - D / 2);

      int sum = 0;
      int count = 0;

      for (; cy < ty; ++cy)
        for (int cx = max (0, 2 * x - D / 2); cx < tx; ++cx)
        {
          int val = src.ptr (cy)[cx];
          if (abs (val - center) < 3 * sigma_color)
          {
            sum += val;
            ++count;
          }
        }
      dst.ptr (y)[x] = sum / count;
    }

	__global__ void
    truncateDepthKernel(PtrStepSz<ushort> depth, ushort max_distance_mm)
	{
		int x = blockIdx.x * blockDim.x + threadIdx.x;
		int y = blockIdx.y * blockDim.y + threadIdx.y;

		if (x < depth.cols && y < depth.rows)		
			if(depth.ptr(y)[x] > max_distance_mm)
				depth.ptr(y)[x] = 0;
	}
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device_64::bilateralFilter (const DepthMap& src, DepthMap& dst)
{
  dim3 block (32, 8);
  dim3 grid (divUp (src.cols (), block.x), divUp (src.rows (), block.y));

  const float sigma_space = sigma_space_640 * (src.cols () / 640.0f);

  cudaFuncSetCacheConfig (bilateralKernel, cudaFuncCachePreferL1);
  bilateralKernel<<<grid, block>>>(src, dst, 0.5f / (sigma_space * sigma_space), 0.5f / (sigma_color * sigma_color));

  cudaSafeCall ( cudaGetLastError () );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device_64::medianFilter3x3 (const DepthMap& src, DepthMap& dst)
{
  dim3 block (MEDIAN_BLOCK_W, MEDIAN_BLOCK_H); // has to be 8x8
  dim3 grid (divUp (src.cols (), block.x), divUp (src.rows (), block.y));

  cudaFuncSetCacheConfig (medianKernel3x3, cudaFuncCachePreferL1);
  medianKernel3x3<<<grid, block>>>(src, dst);

  cudaSafeCall ( cudaGetLastError () );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device_64::pyrDown (const DepthMap& src, DepthMap& dst)
{
  dst.create (src.rows () / 2, src.cols () / 2);

  dim3 block (32, 8);
  dim3 grid (divUp (dst.cols (), block.x), divUp (dst.rows (), block.y));

  //pyrDownGaussKernel<<<grid, block>>>(src, dst, sigma_color);
  pyrDownKernel<<<grid, block>>>(src, dst, sigma_color);
  cudaSafeCall ( cudaGetLastError () );
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void 
pcl::device_64::truncateDepth(DepthMap& depth, float max_distance)
{
  dim3 block (32, 8);
  dim3 grid (divUp (depth.cols (), block.x), divUp (depth.rows (), block.y));

  truncateDepthKernel<<<grid, block>>>(depth, static_cast<ushort>(max_distance * 1000.f));

  cudaSafeCall ( cudaGetLastError () );
}
